<!-- Nav Bar -->
<nav class="navbar navbar-expand navbar-dark bg-dark fixed-top py-3">
      <a class="navbar-brand" href="home.php"><img src="img/dbank.png" class = "logo-cls"></a>

      <div class="collapse navbar-collapse" >
            <ul class="navbar-nav ml-auto">
              <li class="nav-item px-2">
                <a class="nav-link" href="index.php">Home</a>
              </li>
              <li class="nav-item px-2">
                <a class="nav-link" href="transfer.php">Transfer Money</a>
              </li>
              <li class="nav-item px-2">
                <a class="nav-link" href="transactionHistory.php">Transaction History</a>
              </li>
            </ul>  
          </div>
       </nav>
